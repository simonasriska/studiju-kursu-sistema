module com.studyplatform.studyplatform {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;
    requires junit;


    opens com.studyplatform.studyplatform to javafx.fxml;
    exports com.studyplatform.studyplatform;
    exports com.studyplatform.studyplatform.fxControllers;
    opens com.studyplatform.studyplatform.fxControllers to javafx.fxml;
}