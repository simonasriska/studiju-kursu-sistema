package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.databaseControllers.FileDatabaseController;
import com.studyplatform.studyplatform.models.File;
import com.studyplatform.studyplatform.models.Folder;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FolderContentWindow {
    @FXML
    public ListView fileList;

    User currentUser;
    Folder currentFolder;

    public void setupWindow(User currentUser, Folder currentFolder) throws SQLException {
        this.currentUser = currentUser;
        this.currentFolder = currentFolder;
        fetchAllFolderFiles();
    }

    public void fetchAllFolderFiles() throws SQLException {
        fileList.getItems().clear();
        ArrayList<File> allFolderFiles = FileDatabaseController.getAllFolderFiles(currentFolder.getId());
        for (File file : allFolderFiles) {
            fileList.getItems().add(file);
        }
    }

    public void addFile(ActionEvent actionEvent) throws SQLException {
        // TODO: 2021-10-19 Implement with FileChooser
        String fileName = AlertDialog.showTextInput("Enter a file name:");
        if (fileName != null && !fileName.isEmpty()) {
            if (FileDatabaseController.createFile(fileName, "", currentUser.getId(), currentFolder.getId())) {
                AlertDialog.showInformation("File successfully added");
                fetchAllFolderFiles();
            } else {
                AlertDialog.showError("File addition failed");
            }
        }
    }

    public void removeFile(ActionEvent actionEvent) throws SQLException {
        if (fileList.getSelectionModel().getSelectedItem() != null) {
            if (AlertDialog.showConfirmation("Are you sure you want to remove this file?")) {
                File file = (File) fileList.getSelectionModel().getSelectedItem();
                FileDatabaseController.deleteFile(file.getId());
                fileList.getItems().remove(file);
            }
        }
    }

    public void showFileInfo(ActionEvent actionEvent) {
        if (fileList.getSelectionModel().getSelectedItem() != null) {
            File file = (File) fileList.getSelectionModel().getSelectedItem();
            AlertDialog.showInformation(file.showInfo());
        }
    }
}
