package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.StartApplication;
import com.studyplatform.studyplatform.databaseControllers.UserDatabaseController;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.sql.SQLException;

public class LoginWindow {
    @FXML
    public TextField loginField;
    @FXML
    public PasswordField passwordField;

    private User currentUser;

    private void loadMainWindow() throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("main-window.fxml"));
        Parent root = fxmlLoader.load();
        MainWindow mainWindow = fxmlLoader.getController();
        mainWindow.setupWindow(currentUser);
        WindowController.createNewWindow(root, "Study Platform | Current user: " + loginField.getText());
    }

    public void validateUser(ActionEvent actionEvent) throws IOException, SQLException {
        currentUser = UserDatabaseController.getValidatedUser(loginField.getText(), passwordField.getText());
        if (currentUser != null) {
            loadMainWindow();
        } else {
            AlertDialog.showError("Incorrect login details entered");
        }
    }

    public void loadSignUpForm(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("sign-up-form.fxml"));
        Parent root = fxmlLoader.load();
        SignUpForm signUpForm = fxmlLoader.getController();
        signUpForm.setupWindow();
        WindowController.createNewWindow(root, "Study Platform");
    }
}
