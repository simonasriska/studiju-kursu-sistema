package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.StartApplication;
import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.databaseControllers.FolderDatabaseController;
import com.studyplatform.studyplatform.models.Course;
import com.studyplatform.studyplatform.models.Folder;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CourseContentWindow {
    @FXML
    public TreeView folderTree;

    private User currentUser;
    private Course currentCourse;
    private ArrayList<Folder> allCourseFolders;
    private TreeItem rootItem;

    public void setupWindow(User currentUser, Course currentCourse) throws SQLException {
        this.currentUser = currentUser;
        this.currentCourse = currentCourse;
        this.allCourseFolders = new ArrayList<>();
        this.rootItem = new TreeItem(new Folder(0, currentCourse.getTitle(), null, 0, 0, 0));
        rootItem.setExpanded(true);
        folderTree.setRoot(rootItem);
        fetchAllCourseFolders();
    }

    public void fetchAllCourseFolders() throws SQLException {
        allCourseFolders.clear();
        rootItem.getChildren().clear();
        allCourseFolders = FolderDatabaseController.getAllCourseFolders(currentCourse.getId());
        buildFolderTree();
    }

    public void buildFolderTree() {
        generateRootFolders();
        generateSubFolders(rootItem);
    }

    public void generateRootFolders() {
        for (int i = 0; i < allCourseFolders.size(); i++) {
            Folder folder = allCourseFolders.get(i);
            if (folder.getParentId() == 0) {
                TreeItem treeItem = new TreeItem(folder);
                treeItem.setExpanded(true);
                rootItem.getChildren().add(treeItem);
                allCourseFolders.remove(folder);
                i--;
            }
        }
    }

    public void generateSubFolders(TreeItem treeItem) {
        for (int i = 0; i < treeItem.getChildren().size(); i++) {
            TreeItem currentTreeItem = (TreeItem) treeItem.getChildren().get(i);
            Folder currentFolder = (Folder) currentTreeItem.getValue();
            for (int j = 0; j < allCourseFolders.size(); j++) {
                Folder folder = allCourseFolders.get(j);
                if (folder.getParentId() == currentFolder.getId()) {
                    TreeItem newTreeItem = new TreeItem(folder);
                    newTreeItem.setExpanded(true);
                    currentTreeItem.getChildren().add(newTreeItem);
                    allCourseFolders.remove(folder);
                    j--;
                    generateSubFolders(currentTreeItem);
                }
            }
        }
    }

    public void showFolderContent(ActionEvent actionEvent) throws IOException, SQLException {
        if (folderTree.getSelectionModel().getSelectedItem() != null && folderTree.getSelectionModel().getSelectedItem() != rootItem) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("folder-content-window.fxml"));
            Parent root = fxmlLoader.load();
            FolderContentWindow folderContentWindow = fxmlLoader.getController();
            TreeItem treeItem = (TreeItem) folderTree.getSelectionModel().getSelectedItem();
            Folder folder = (Folder) treeItem.getValue();
            folderContentWindow.setupWindow(currentUser, folder);
            WindowController.createNewWindow(root, "Study Platform");
        }
    }

    public void createFolder(ActionEvent actionEvent) throws IOException, SQLException {
        if (folderTree.getSelectionModel().getSelectedItem() != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("folder-edit-form.fxml"));
            Parent root = fxmlLoader.load();
            FolderEditForm folderEditForm = fxmlLoader.getController();
            TreeItem treeItem = (TreeItem) folderTree.getSelectionModel().getSelectedItem();
            Folder folder = (Folder) treeItem.getValue();
            folderEditForm.setupWindow(currentUser, currentCourse, null, folder);
            WindowController.createNewWindow(root, "Study Platform");
            fetchAllCourseFolders();
        }
    }

    public void editFolder(ActionEvent actionEvent) throws IOException, SQLException {
        if (folderTree.getSelectionModel().getSelectedItem() != null && folderTree.getSelectionModel().getSelectedItem() != rootItem) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("folder-edit-form.fxml"));
            Parent root = fxmlLoader.load();
            FolderEditForm folderEditForm = fxmlLoader.getController();
            TreeItem treeItem = (TreeItem) folderTree.getSelectionModel().getSelectedItem();
            Folder folder = (Folder) treeItem.getValue();
            folderEditForm.setupWindow(currentUser, currentCourse, folder, null);
            WindowController.createNewWindow(root, "Study Platform");
            fetchAllCourseFolders();
        }
    }

    public void deleteFolder(ActionEvent actionEvent) throws SQLException {
        if (folderTree.getSelectionModel().getSelectedItem() != null && folderTree.getSelectionModel().getSelectedItem() != rootItem) {
            if (AlertDialog.showConfirmation("Are you sure you want to delete this folder?")) {
                TreeItem treeItem = (TreeItem) folderTree.getSelectionModel().getSelectedItem();
                Folder folder = (Folder) treeItem.getValue();
                FolderDatabaseController.deleteFolder(folder.getId());
                fetchAllCourseFolders();
            }
        }
    }

    public void showFolderInfo(ActionEvent actionEvent) {
        if (folderTree.getSelectionModel().getSelectedItem() != null && folderTree.getSelectionModel().getSelectedItem() != rootItem) {
            TreeItem treeItem = (TreeItem) folderTree.getSelectionModel().getSelectedItem();
            Folder folder = (Folder) treeItem.getValue();
            AlertDialog.showInformation(folder.showInfo());
        }
    }
}
