package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.StartApplication;
import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.databaseControllers.CourseDatabaseController;
import com.studyplatform.studyplatform.databaseControllers.UserDatabaseController;
import com.studyplatform.studyplatform.models.Company;
import com.studyplatform.studyplatform.models.Course;
import com.studyplatform.studyplatform.models.Person;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MainWindow {
    @FXML
    public ListView userList;
    @FXML
    public TabPane tabPane;
    @FXML
    public ListView courseList;
    @FXML
    public ListView unenrolledCourseList;
    @FXML
    public ListView enrolledCourseList;
    @FXML
    public Button courseCreateButton;
    @FXML
    public Button courseEditButton;
    @FXML
    public Button courseDeleteButton;

    private User currentUser;

    public void setupWindow(User currentUser) throws SQLException {
        this.currentUser = currentUser;
        if (currentUser.getLogin().equals("admin")) {
            fetchAllUsers();
        } else {
            tabPane.getTabs().remove(3);
        }
        fetchAllCourses();
        fetchUserCourses();
    }

    public void fetchUserCourses() throws SQLException {
        unenrolledCourseList.getItems().clear();
        enrolledCourseList.getItems().clear();
        ArrayList<Course> unenrolledCourses = CourseDatabaseController.getUserCourses("unenrolled", currentUser.getId());
        ArrayList<Course> enrolledCourses = CourseDatabaseController.getUserCourses("enrolled", currentUser.getId());
        for (Course course : unenrolledCourses) {
            unenrolledCourseList.getItems().add(course);
        }
        for (Course course : enrolledCourses) {
            enrolledCourseList.getItems().add(course);
        }
    }

    public void fetchAllCourses() throws SQLException {
        courseList.getItems().clear();
        ArrayList<Course> allCourses = CourseDatabaseController.getAllCourses();
        for (Course course : allCourses) {
            courseList.getItems().add(course);
        }
    }

    public void fetchAllUsers() throws SQLException {
        userList.getItems().clear();
        ArrayList<User> allUsers = UserDatabaseController.getAllUsers(currentUser.getId());
        for (User user : allUsers) {
            userList.getItems().add(user);
        }
    }

    public void checkRights(MouseEvent mouseEvent) {
        if (courseList.getSelectionModel().getSelectedItem() != null) {
            Course course = (Course) courseList.getSelectionModel().getSelectedItem();
            if (currentUser.getId() != course.getCreatorId() && !currentUser.getLogin().equals("admin")) {
                courseEditButton.setDisable(true);
                courseDeleteButton.setDisable(true);
            } else {
                courseEditButton.setDisable(false);
                courseDeleteButton.setDisable(false);
            }
        }
    }

    public void editUser(ActionEvent actionEvent) throws IOException, SQLException {
        if (userList.getSelectionModel().getSelectedItem() != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("user-edit-form.fxml"));
            Parent root = fxmlLoader.load();
            UserEditForm userEditForm = fxmlLoader.getController();
            User selectedUser = (User) userList.getSelectionModel().getSelectedItem();
            userEditForm.setupWindow(selectedUser);
            WindowController.createNewWindow(root, "Study Platform");
            fetchAllUsers();
        }
    }

    public void deleteUser(ActionEvent actionEvent) throws SQLException {
        if (userList.getSelectionModel().getSelectedItem() != null) {
            if (AlertDialog.showConfirmation("Are you sure you want to delete this user?")) {
                User user = (User) userList.getSelectionModel().getSelectedItem();
                UserDatabaseController.deleteUser(user.getId());
                userList.getItems().remove(user);
            }
        }
    }

    public void showUserInfo(ActionEvent actionEvent) throws SQLException {
        if (userList.getSelectionModel().getSelectedItem() != null) {
            User user = (User) userList.getSelectionModel().getSelectedItem();
            AlertDialog.showInformation(user.showInfo());
        }
    }


    public void showCourseContent(ActionEvent actionEvent) throws IOException, SQLException {
        if (courseList.getSelectionModel().getSelectedItem() != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("course-content-window.fxml"));
            Parent root = fxmlLoader.load();
            CourseContentWindow courseContentWindow = fxmlLoader.getController();
            Course course = (Course) courseList.getSelectionModel().getSelectedItem();
            courseContentWindow.setupWindow(currentUser, course);
            WindowController.createNewWindow(root, "Study Platform");
        }
    }

    public void editCourse(ActionEvent actionEvent) throws IOException, SQLException {
        if (courseList.getSelectionModel().getSelectedItem() != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("course-edit-form.fxml"));
            Parent root = fxmlLoader.load();
            CourseEditForm courseEditForm = fxmlLoader.getController();
            Course course = (Course) courseList.getSelectionModel().getSelectedItem();
            courseEditForm.setupWindow(course, currentUser);
            WindowController.createNewWindow(root, "Study Platform");
            fetchAllCourses();
        }
    }

    public void deleteCourse(ActionEvent actionEvent) throws SQLException {
        if (courseList.getSelectionModel().getSelectedItem() != null) {
            if (AlertDialog.showConfirmation("Are you sure you want to delete this course?")) {
                Course course = (Course) courseList.getSelectionModel().getSelectedItem();
                CourseDatabaseController.deleteCourse(course.getId());
                courseList.getItems().remove(course);
            }
        }
    }

    public void showCourseInfo(ActionEvent actionEvent) {
        if (courseList.getSelectionModel().getSelectedItem() != null) {
            Course course = (Course) courseList.getSelectionModel().getSelectedItem();
            AlertDialog.showInformation(course.showInfo());
        }
    }

    public void createCourse(ActionEvent actionEvent) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("course-edit-form.fxml"));
        Parent root = fxmlLoader.load();
        CourseEditForm courseEditForm = fxmlLoader.getController();
        courseEditForm.setupWindow(null, currentUser);
        WindowController.createNewWindow(root, "Study Platform");
        fetchAllCourses();
    }

    public void enrollCourse(ActionEvent actionEvent) throws SQLException {
        if (unenrolledCourseList.getSelectionModel().getSelectedItem() != null) {
            Course course = (Course) unenrolledCourseList.getSelectionModel().getSelectedItem();
            CourseDatabaseController.enroll(currentUser.getId(), course.getId());
            fetchUserCourses();
        }
    }

    public void unenrollCourse(ActionEvent actionEvent) throws SQLException {
        if (enrolledCourseList.getSelectionModel().getSelectedItem() != null) {
            Course course = (Course) enrolledCourseList.getSelectionModel().getSelectedItem();
            CourseDatabaseController.unenroll(currentUser.getId(), course.getId());
            fetchUserCourses();
        }
    }

    public void editAccount(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("user-edit-form.fxml"));
        Parent root = fxmlLoader.load();
        UserEditForm userEditForm = fxmlLoader.getController();
        userEditForm.setupWindow(currentUser);
        WindowController.createNewWindow(root, "Study Platform");
    }

    public void deleteAccount(ActionEvent actionEvent) throws SQLException {
        if (AlertDialog.showConfirmation("Are you sure you want to delete this user?")) {
            UserDatabaseController.deleteUser(currentUser.getId());
            Stage stage = (Stage) tabPane.getScene().getWindow();
            stage.close();
        }
    }
}
