package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.databaseControllers.CourseDatabaseController;
import com.studyplatform.studyplatform.models.Course;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;

public class CourseEditForm {
    @FXML
    public TextField titleField;
    @FXML
    public TextArea descriptionField;

    private Course course;
    private User currentUser;

    public void setupWindow(Course course, User currentUser) {
        this.course = course;
        this.currentUser = currentUser;
        if (this.course != null) {
            titleField.setText(course.getTitle());
            descriptionField.setText(course.getDescription());
        }
    }

    public void updateCourse(ActionEvent actionEvent) throws SQLException {
        if (course == null) {
            if (CourseDatabaseController.createCourse(titleField.getText(), descriptionField.getText(), currentUser.getId())) {
                AlertDialog.showInformation("Course successfully created");
                closeCurrentWindow();
            } else {
                AlertDialog.showError("Course creation failed");
            }
        } else {
            CourseDatabaseController.updateUser(course.getId(), titleField.getText(), descriptionField.getText());
            AlertDialog.showInformation("Course successfully updated");
            closeCurrentWindow();
        }
    }

    public void returnToPreviousWindow(ActionEvent actionEvent) {
        closeCurrentWindow();
    }

    private void closeCurrentWindow() {
        Stage stage = (Stage) titleField.getScene().getWindow();
        stage.close();
    }
}
