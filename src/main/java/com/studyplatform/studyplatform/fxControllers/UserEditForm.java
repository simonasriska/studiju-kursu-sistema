package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.databaseControllers.UserDatabaseController;
import com.studyplatform.studyplatform.models.Company;
import com.studyplatform.studyplatform.models.Person;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.SQLException;

public class UserEditForm {
    @FXML
    public PasswordField passwordField;
    @FXML
    public Label userInfoLabel1;
    @FXML
    public TextField userInfoField1;
    @FXML
    public Label userInfoLabel2;
    @FXML
    public TextField userInfoField2;
    @FXML
    public Label userInfoLabel3;
    @FXML
    public TextField userInfoField3;

    private User selectedUser;
    private Person person;
    private Company company;

    public void setupWindow(User selectedUser) {
        this.selectedUser = selectedUser;
        if (selectedUser.getClass().getSimpleName().equals("Person")) {
            userInfoLabel1.setText("Name:");
            userInfoLabel2.setText("Surname:");
            userInfoLabel3.setText("Phone:");
            person = (Person) selectedUser;
            userInfoField1.setText(person.getName());
            userInfoField2.setText(person.getSurname());
            userInfoField3.setText(person.getPhone());
        } else {
            userInfoLabel1.setText("Name:");
            userInfoLabel2.setText("Representative:");
            userInfoLabel3.setText("Email:");
            company = (Company) selectedUser;
            userInfoField1.setText(company.getName());
            userInfoField2.setText(company.getRepresentative());
            userInfoField3.setText(company.getEmail());
        }
    }

    public void updateUser(ActionEvent actionEvent) throws SQLException {
        String newPassword;
        if (passwordField.getText().isEmpty()) {
            newPassword = selectedUser.getPassword();
        } else {
            newPassword = passwordField.getText();
        }
        UserDatabaseController.updateUser(
                selectedUser.getId(),
                selectedUser.getClass().getSimpleName().equals("Person"),
                newPassword,
                userInfoField1.getText(),
                userInfoField2.getText(),
                userInfoField3.getText()
        );
        AlertDialog.showInformation("User successfully updated");
        closeCurrentWindow();
    }

    private void closeCurrentWindow() {
        Stage stage = (Stage) passwordField.getScene().getWindow();
        stage.close();
    }

    public void returnToPreviousWindow(ActionEvent actionEvent) {
        closeCurrentWindow();
    }
}
