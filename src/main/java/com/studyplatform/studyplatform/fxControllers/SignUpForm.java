package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.databaseControllers.UserDatabaseController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class SignUpForm {
    @FXML
    public TextField loginField;
    @FXML
    public PasswordField passwordField;
    @FXML
    public RadioButton personButton;
    @FXML
    public ToggleGroup userGroup;
    @FXML
    public RadioButton companyButton;
    @FXML
    public Label userInfoLabel1;
    @FXML
    public TextField userInfoField1;
    @FXML
    public Label userInfoLabel2;
    @FXML
    public TextField userInfoField2;
    @FXML
    public Label userInfoLabel3;
    @FXML
    public TextField userInfoField3;

    public void setupWindow() {
        userInfoLabel1.setText("Name:");
        userInfoLabel2.setText("Surname:");
        userInfoLabel3.setText("Phone:");
    }

    public void createUser(ActionEvent actionEvent) {
        if (UserDatabaseController.createUser(
                personButton.isSelected(),
                loginField.getText(),
                passwordField.getText(),
                userInfoField1.getText(),
                userInfoField2.getText(),
                userInfoField3.getText()
        )) {
            AlertDialog.showInformation("Account successfully created");
            closeCurrentWindow();
        } else {
            AlertDialog.showError("Account creation failed");
        }
    }

    private void closeCurrentWindow() {
        Stage stage = (Stage) loginField.getScene().getWindow();
        stage.close();
    }

    public void returnToPreviousWindow(ActionEvent actionEvent) throws IOException {
        closeCurrentWindow();
    }

    public void changeInfoLabels(ActionEvent actionEvent) {
        if (personButton.isSelected()) {
            userInfoLabel1.setText("Name:");
            userInfoLabel2.setText("Surname:");
            userInfoLabel3.setText("Phone:");
        } else {
            userInfoLabel1.setText("Name:");
            userInfoLabel2.setText("Representative:");
            userInfoLabel3.setText("Email:");
        }
    }
}
