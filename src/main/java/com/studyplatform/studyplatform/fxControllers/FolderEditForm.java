package com.studyplatform.studyplatform.fxControllers;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.databaseControllers.FolderDatabaseController;
import com.studyplatform.studyplatform.models.Course;
import com.studyplatform.studyplatform.models.Folder;
import com.studyplatform.studyplatform.models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class FolderEditForm {
    @FXML
    public TextField nameField;
    @FXML
    public TextArea descriptionField;

    private User currentUser;
    private Course currentCourse;
    private Folder currentFolder;
    private Folder parentFolder;

    public void setupWindow(User currentUser, Course currentCourse, Folder currentFolder, Folder parentFolder) {
        this.currentUser = currentUser;
        this.currentCourse = currentCourse;
        this.currentFolder = currentFolder;
        this.parentFolder = parentFolder;
        if (this.currentFolder != null) {
            nameField.setText(currentFolder.getName());
            descriptionField.setText(currentFolder.getDescription());
        }
    }

    public void updateFolder(ActionEvent actionEvent) throws SQLException {
        if (currentFolder == null) {
            if (FolderDatabaseController.createFolder(
                    nameField.getText(),
                    descriptionField.getText(),
                    currentUser.getId(),
                    currentCourse.getId(),
                    parentFolder.getId()
            )) {
                AlertDialog.showInformation("Folder successfully created");
                closeCurrentWindow();
            } else {
                AlertDialog.showError("Folder creation failed");
            }
        } else {
            FolderDatabaseController.updateFolder(currentFolder.getId(), nameField.getText(), descriptionField.getText());
            AlertDialog.showInformation("Folder successfully updated");
            closeCurrentWindow();
        }
    }

    public void returnToPreviousWindow(ActionEvent actionEvent) {
        closeCurrentWindow();
    }

    private void closeCurrentWindow() {
        Stage stage = (Stage) nameField.getScene().getWindow();
        stage.close();
    }
}
