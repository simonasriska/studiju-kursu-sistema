package com.studyplatform.studyplatform.controllers;

import com.studyplatform.studyplatform.fxControllers.AlertDialog;

import java.sql.*;

public class DatabaseController {
    public static Connection connectToDatabase() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String DB_URL = "jdbc:mysql://localhost/study_platform";
            String USER = "root";
            String PASS = "";
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (Exception e) {
            AlertDialog.showError("Failed to connect to database");
        }
        return connection;
    }

    public static void disconnectFromDatabase(Connection connection, Statement statement, ResultSet resultSet) throws SQLException {
        if (connection != null) {
            connection.close();
        }
        if (statement != null) {
            statement.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
    }
}
