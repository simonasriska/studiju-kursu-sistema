package com.studyplatform.studyplatform.controllers;

import com.studyplatform.studyplatform.models.StudySystem;

import java.io.*;

public class DataController {
    public static StudySystem loadFromFile(String fileName) {
        StudySystem studySystem = null;
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName));
            studySystem = (StudySystem) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {

        }
        return studySystem;
    }

    public static void writeToFile(String fileName, StudySystem studySystem) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName));
            objectOutputStream.writeObject(studySystem);
            objectOutputStream.close();
        } catch (Exception e) {

        }
    }
}
