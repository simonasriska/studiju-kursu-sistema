package com.studyplatform.studyplatform;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class StartApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException, SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        if (connection != null) {
            DatabaseController.disconnectFromDatabase(connection, null, null);
            FXMLLoader fxmlLoader = new FXMLLoader(StartApplication.class.getResource("login-window.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Study Platform");
            stage.setScene(scene);
            stage.show();
        }
    }

    public static void main(String[] args) {
        launch();
    }
}