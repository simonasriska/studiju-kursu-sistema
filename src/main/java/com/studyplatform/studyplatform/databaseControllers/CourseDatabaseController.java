package com.studyplatform.studyplatform.databaseControllers;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.fxControllers.AlertDialog;
import com.studyplatform.studyplatform.models.Course;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CourseDatabaseController {
    public static ArrayList<Course> getAllCourses() throws SQLException {
        ArrayList<Course> allCourses = new ArrayList<>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectString = "SELECT * FROM course";
        PreparedStatement preparedStatement = connection.prepareStatement(selectString);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Course course = new Course(
                    resultSet.getInt("id"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    resultSet.getInt("creator_id")
            );
            allCourses.add(course);
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, resultSet);
        return allCourses;
    }

    public static boolean createCourse(String titleField, String descriptionField, int creatorId) {
        boolean isCourseCreated;
        String insertString = "INSERT INTO course(`title`, `description`, `creator_id`) VALUES(?, ?, ?)";
        try {
            Connection connection = DatabaseController.connectToDatabase();
            PreparedStatement preparedStatement = connection.prepareStatement(insertString);
            preparedStatement.setString(1, titleField);
            preparedStatement.setString(2, descriptionField);
            preparedStatement.setInt(3, creatorId);
            preparedStatement.execute();
            DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
            isCourseCreated = true;
        } catch (SQLException e) {
            isCourseCreated = false;
        }
        return isCourseCreated;
    }

    public static void updateUser(int courseId, String titleField, String descriptionField) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String updateString = "UPDATE course SET title = ?, description = ? WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(updateString);
        preparedStatement.setString(1, titleField);
        preparedStatement.setString(2, descriptionField);
        preparedStatement.setInt(3, courseId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }

    public static void deleteCourse(int courseId) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteString = "DELETE FROM course WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteString);
        preparedStatement.setInt(1, courseId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }

    public static ArrayList<Course> getUserCourses(String type, int userId) throws SQLException {
        ArrayList<Course> unenrolledCourses = new ArrayList<>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectString;
        if (type == "unenrolled") {
            selectString = "SELECT * FROM course WHERE creator_id != ? AND id NOT IN (SELECT course_id FROM participant WHERE user_id = ?)";
        } else {
            selectString = "SELECT * FROM course WHERE creator_id != ? AND id IN (SELECT course_id FROM participant WHERE user_id = ?)";
        }
        PreparedStatement preparedStatement = connection.prepareStatement(selectString);
        preparedStatement.setInt(1, userId);
        preparedStatement.setInt(2, userId);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Course course = new Course(
                    resultSet.getInt("id"),
                    resultSet.getString("title"),
                    resultSet.getString("description"),
                    resultSet.getInt("creator_id")
            );
            unenrolledCourses.add(course);
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, resultSet);
        return unenrolledCourses;
    }

    public static void enroll(int userId, int courseId) throws SQLException {
        String insertString = "INSERT INTO participant(`user_id`, `course_id`) VALUES(?, ?)";
        Connection connection = DatabaseController.connectToDatabase();
        PreparedStatement preparedStatement = connection.prepareStatement(insertString);
        preparedStatement.setInt(1, userId);
        preparedStatement.setInt(2, courseId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }

    public static void unenroll(int userId, int courseId) throws SQLException {
        String insertString = "DELETE FROM participant WHERE user_id = ? AND course_id = ?";
        Connection connection = DatabaseController.connectToDatabase();
        PreparedStatement preparedStatement = connection.prepareStatement(insertString);
        preparedStatement.setInt(1, userId);
        preparedStatement.setInt(2, courseId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }
}
