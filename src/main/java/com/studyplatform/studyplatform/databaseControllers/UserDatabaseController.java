package com.studyplatform.studyplatform.databaseControllers;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.fxControllers.AlertDialog;
import com.studyplatform.studyplatform.models.Company;
import com.studyplatform.studyplatform.models.Person;
import com.studyplatform.studyplatform.models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDatabaseController {
    public static User getValidatedUser(String loginField, String passwordField) throws SQLException {
        User currentUser = null;
        Connection connection = DatabaseController.connectToDatabase();
        String selectString = "SELECT * FROM user WHERE login = ? AND password = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectString);
        preparedStatement.setString(1, loginField);
        preparedStatement.setString(2, passwordField);
        ResultSet resultSet = preparedStatement.executeQuery();
        int id = 0;
        String login = null;
        String password = null;
        String personName = null;
        String personSurname = null;
        String personPhone = null;
        String companyName = null;
        String companyRepresentative = null;
        String companyEmail = null;
        while (resultSet.next()) {
            id = resultSet.getInt("id");
            login = resultSet.getString("login");
            password = resultSet.getString("password");
            personName = resultSet.getString("person_name");
            personSurname = resultSet.getString("person_surname");
            personPhone = resultSet.getString("person_phone");
            companyName = resultSet.getString("company_name");
            companyRepresentative = resultSet.getString("company_representative");
            companyEmail = resultSet.getString("company_email");
        }
        if (id != 0) {
            if (personName != null) {
                currentUser = new Person(id, login, password, personName, personSurname, personPhone);
            } else {
                currentUser = new Company(id, login, password, companyName, companyRepresentative, companyEmail);
            }
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, resultSet);
        return currentUser;
    }

    public static boolean createUser(boolean isPerson, String loginField, String passwordField, String userInfoField1, String userInfoField2, String userInfoField3) {
        boolean isUserCreated;
        String insertString;
        if (isPerson) {
            insertString = "INSERT INTO user(`login`, `password`, `person_name`, `person_surname`, `person_phone`) VALUES(?, ?, ?, ?, ?)";
        } else {
            insertString = "INSERT INTO user(`login`, `password`, `company_name`, `company_representative`, `company_email`) VALUES(?, ?, ?, ?, ?)";
        }
        try {
            Connection connection = DatabaseController.connectToDatabase();
            PreparedStatement preparedStatement = connection.prepareStatement(insertString);
            preparedStatement.setString(1, loginField);
            preparedStatement.setString(2, passwordField);
            preparedStatement.setString(3, userInfoField1);
            preparedStatement.setString(4, userInfoField2);
            preparedStatement.setString(5, userInfoField3);
            preparedStatement.execute();
            DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
            isUserCreated = true;
        } catch (SQLException e) {
            isUserCreated = false;
        }
        return isUserCreated;
    }

    public static ArrayList<User> getAllUsers(int excludedUserId) throws SQLException {
        ArrayList<User> allUsers = new ArrayList<>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectString = "SELECT * FROM user WHERE id != ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectString);
        preparedStatement.setInt(1, excludedUserId);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            String personName = resultSet.getString("person_name");
            String personSurname = resultSet.getString("person_surname");
            String personPhone = resultSet.getString("person_phone");
            String companyName = resultSet.getString("company_name");
            String companyRepresentative = resultSet.getString("company_representative");
            String companyEmail = resultSet.getString("company_email");
            User user = null;
            if (personName != null) {
                user = new Person(id, login, password, personName, personSurname, personPhone);
            } else {
                user = new Company(id, login, password, companyName, companyRepresentative, companyEmail);
            }
            allUsers.add(user);
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, resultSet);
        return allUsers;
    }

    public static void updateUser(int userId, boolean isPerson, String passwordField, String userInfoField1, String userInfoField2, String userInfoField3) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String updateString;
        if (isPerson) {
            updateString = "UPDATE user SET password = ?, person_name = ?, person_surname = ?, person_phone = ? WHERE id = ?";
        } else {
            updateString = "UPDATE user SET password = ?, company_name = ?, company_representative = ?, company_email = ? WHERE id = ?";
        }
        PreparedStatement preparedStatement = connection.prepareStatement(updateString);
        preparedStatement.setString(1, passwordField);
        preparedStatement.setString(2, userInfoField1);
        preparedStatement.setString(3, userInfoField2);
        preparedStatement.setString(4, userInfoField3);
        preparedStatement.setInt(5, userId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }



    public static void deleteUser(int userId) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteString = "DELETE FROM user WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteString);
        preparedStatement.setInt(1, userId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }
}
