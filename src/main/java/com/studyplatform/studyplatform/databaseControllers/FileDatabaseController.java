package com.studyplatform.studyplatform.databaseControllers;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.fxControllers.AlertDialog;
import com.studyplatform.studyplatform.models.File;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FileDatabaseController {
    public static ArrayList<File> getAllFolderFiles(int folderId) throws SQLException {
        ArrayList<File> allFolderFiles = new ArrayList<>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectString = "SELECT * FROM file WHERE folder_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectString);
        preparedStatement.setInt(1, folderId);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            File file = new File(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("path"),
                    resultSet.getInt("creator_id"),
                    resultSet.getInt("folder_id")
            );
            allFolderFiles.add(file);
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, resultSet);
        return allFolderFiles;
    }

    public static boolean createFile(String fileName, String path, int creatorId, int folderId) {
        boolean isFileCreated;
        String insertString = "INSERT INTO file(`name`, `path`, `creator_id`, `folder_id`) VALUES(?, ?, ?, ?)";
        try {
            Connection connection = DatabaseController.connectToDatabase();
            PreparedStatement preparedStatement = connection.prepareStatement(insertString);
            preparedStatement.setString(1, fileName);
            preparedStatement.setString(2, path);
            preparedStatement.setInt(3, creatorId);
            preparedStatement.setInt(4, folderId);
            preparedStatement.execute();
            DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
            isFileCreated = true;
        } catch (SQLException e) {
            isFileCreated = false;
        }
        return isFileCreated;
    }

    public static void deleteFile(int fileId) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteString = "DELETE FROM file WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteString);
        preparedStatement.setInt(1, fileId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }
}
