package com.studyplatform.studyplatform.databaseControllers;

import com.studyplatform.studyplatform.controllers.DatabaseController;
import com.studyplatform.studyplatform.fxControllers.AlertDialog;
import com.studyplatform.studyplatform.models.Folder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FolderDatabaseController {
    public static ArrayList<Folder> getAllCourseFolders(int courseId) throws SQLException {
        ArrayList<Folder> allCourseFolders = new ArrayList<>();
        Connection connection = DatabaseController.connectToDatabase();
        String selectString = "SELECT * FROM folder WHERE course_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(selectString);
        preparedStatement.setInt(1, courseId);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Folder folder = new Folder(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("description"),
                    resultSet.getInt("creator_id"),
                    resultSet.getInt("course_id"),
                    resultSet.getInt("parent_id")
            );
            allCourseFolders.add(folder);
        }
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, resultSet);
        return allCourseFolders;
    }

    public static boolean createFolder(String nameField, String descriptionField, int creatorId, int courseId, int parentId) {
        boolean isFolderCreated;
        String insertString = "INSERT INTO folder(`name`, `description`, `creator_id`, `course_id`, `parent_id`) VALUES(?, ?, ?, ?, ?)";
        try {
            Connection connection = DatabaseController.connectToDatabase();
            PreparedStatement preparedStatement = connection.prepareStatement(insertString);
            preparedStatement.setString(1, nameField);
            preparedStatement.setString(2, descriptionField);
            preparedStatement.setInt(3, creatorId);
            preparedStatement.setInt(4, courseId);
            preparedStatement.setInt(5, parentId);
            preparedStatement.execute();
            DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
            isFolderCreated = true;
        } catch (SQLException e) {
            isFolderCreated = false;
        }
        return isFolderCreated;
    }

    public static void updateFolder(int folderId, String nameField, String descriptionField) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String updateString = "UPDATE folder SET name = ?, description = ? WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(updateString);
        preparedStatement.setString(1, nameField);
        preparedStatement.setString(2, descriptionField);
        preparedStatement.setInt(3, folderId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }

    public static void deleteFolder(int folderId) throws SQLException {
        Connection connection = DatabaseController.connectToDatabase();
        String deleteString = "DELETE FROM folder WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteString);
        preparedStatement.setInt(1, folderId);
        preparedStatement.execute();
        DatabaseController.disconnectFromDatabase(connection, preparedStatement, null);
    }
}
