package com.studyplatform.studyplatform;

import com.studyplatform.studyplatform.databaseControllers.CourseDatabaseController;
import com.studyplatform.studyplatform.databaseControllers.UserDatabaseController;
import com.studyplatform.studyplatform.models.Course;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class Testing {

    @Test
    public void getUnenrolledUserCourses() throws SQLException {
        // arrange
        String courseType = "unenrolled";
        int userId = 4, expectedCourseCount = 1;

        // act
        ArrayList<Course> unenrolledCourses = CourseDatabaseController.getUserCourses(courseType, userId);
        int actualCourseCount = unenrolledCourses.size();

        // assert
        assertEquals(expectedCourseCount, actualCourseCount);
    }

    @Test
    public void getEnrolledUserCourses() throws SQLException {
        // arrange
        String courseType = "enrolled";
        int userId = 4, expectedCourseCount = 0;

        // act
        ArrayList<Course> unenrolledCourses = CourseDatabaseController.getUserCourses(courseType, userId);
        int actualCourseCount = unenrolledCourses.size();

        // assert
        assertEquals(expectedCourseCount, actualCourseCount);
    }

    @Test
    public void updateCurrentUser() throws SQLException {
        // arrange
        int userId = 10;
        boolean isPerson = true;
        String password = "TestPassword";
        String name = "TestName";
        String surname = "TestSurname";
        String phone = "TestPhone";

        // act
        boolean isUpdated = false;
        try {
            UserDatabaseController.updateUser(userId, isPerson, password, name, surname, phone);
            isUpdated = true;
        } catch (SQLException e) {
            isUpdated = false;
        }

        // assert
        assertTrue(isUpdated);
    }


}