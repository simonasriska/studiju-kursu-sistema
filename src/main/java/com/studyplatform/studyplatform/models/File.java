package com.studyplatform.studyplatform.models;

public class File {
    private int id;
    private String name;
    private String path;
    private int creatorId;
    private int folderId;

    public File(int id, String name, String path, int creatorId, int folderId) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.creatorId = creatorId;
        this.folderId = folderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getFolderId() {
        return folderId;
    }

    public void setFolderId(int folderId) {
        this.folderId = folderId;
    }

    @Override
    public String toString() {
        return name;
    }

    public String showInfo() {
        return "ID: " + id + "\n" +
                "Name: " + name + "\n" +
                "Path: " + path + "\n" +
                "Creator ID: " + creatorId + "\n" +
                "Folder ID: " + folderId + "\n";
    }
}
