package com.studyplatform.studyplatform.models;

public class Person extends User{
    private String name;
    private String surname;
    private String phone;

    public Person(int id, String login, String password, String name, String surname, String phone) {
        super(id, login, password);
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return getLogin();
    }

    @Override
    public String showInfo() {
        return "ID: " + getId() + "\n" +
                "Login: " + getLogin() + "\n" +
                "Password: " + getPassword() + "\n" +
                "Person name: " + name + "\n" +
                "Person surname: " + surname + "\n" +
                "Person phone: " + phone + "\n";
    }
}
