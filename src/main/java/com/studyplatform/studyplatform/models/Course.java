package com.studyplatform.studyplatform.models;

import java.util.ArrayList;

public class Course {
    private int id;
    private String title;
    private String description;
    private int creatorId;
    private ArrayList<User> moderators;
    private ArrayList<User> participants;

    public Course(int id, String title, String description, int creatorId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.creatorId = creatorId;
        this.moderators = new ArrayList<>();
        this.participants = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public ArrayList<User> getModerators() {
        return moderators;
    }

    public void setModerators(ArrayList<User> moderators) {
        this.moderators = moderators;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<User> participants) {
        this.participants = participants;
    }

    @Override
    public String toString() {
        return title;
    }

    public String showInfo() {
        return "ID: " + id + "\n" +
                "Title: " + title + "\n" +
                "Description: " + description + "\n" +
                "Creator ID: " + creatorId + "\n";
    }
}
