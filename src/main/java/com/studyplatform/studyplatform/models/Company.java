package com.studyplatform.studyplatform.models;

public class Company extends User{
    private String name;
    private String representative;
    private String email;

    public Company(int id, String login, String password, String name, String representative, String email) {
        super(id, login, password);
        this.name = name;
        this.representative = representative;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return getLogin();
    }

    @Override
    public String showInfo() {
        return "ID: " + getId() + "\n" +
                "Login: " + getLogin() + "\n" +
                "Password: " + getPassword() + "\n" +
                "Company name: " + name + "\n" +
                "Company representative: " + representative + "\n" +
                "Company email: " + email + "\n";
    }
}
