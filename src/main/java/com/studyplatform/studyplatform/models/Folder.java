package com.studyplatform.studyplatform.models;

import java.util.ArrayList;

public class Folder {
    private int id;
    private String name;
    private String description;
    private int creatorId;
    private int courseId;
    private int parentId;
    private ArrayList<Folder> subFolders;
    private ArrayList<File> files;

    public Folder(int id, String name, String description, int creatorId, int courseId, int parentId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creatorId = creatorId;
        this.courseId = courseId;
        this.parentId = parentId;
        this.subFolders = new ArrayList<>();
        this.files = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public ArrayList<Folder> getSubFolders() {
        return subFolders;
    }

    public void setSubFolders(ArrayList<Folder> subFolders) {
        this.subFolders = subFolders;
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<File> files) {
        this.files = files;
    }

    @Override
    public String toString() {
        return name;
    }

    public String showInfo() {
        return "ID: " + id + "\n" +
                "Name: " + name + "\n" +
                "Description: " + description + "\n" +
                "Creator ID: " + creatorId + "\n" +
                "Course ID: " + courseId + "\n" +
                "Parent ID: " + parentId + "\n";
    }
}
