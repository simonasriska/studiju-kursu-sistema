package com.studyplatform.studyplatform.models;

import java.util.ArrayList;

public class StudySystem {
    private ArrayList<User> allSystemUsers;
    private ArrayList<Course> allSystemCourses;

    public StudySystem() {
        this.allSystemUsers = new ArrayList<>();
        this.allSystemCourses = new ArrayList<>();
    }

    public ArrayList<User> getAllSystemUsers() {
        return allSystemUsers;
    }

    public void setAllSystemUsers(ArrayList<User> allSystemUsers) {
        this.allSystemUsers = allSystemUsers;
    }

    public ArrayList<Course> getAllSystemCourses() {
        return allSystemCourses;
    }

    public void setAllSystemCourses(ArrayList<Course> allSystemCourses) {
        this.allSystemCourses = allSystemCourses;
    }
}
